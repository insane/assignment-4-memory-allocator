#include "tests.h"

int main() {
	test_mem_malloc();
	test_free_block();
	test_free_blocks();
	test_new_region();
	test_new_region_in_other_place();
	return 0;
}