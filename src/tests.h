#define HEAP_SIZE 4096
#define BLOCK_DEF_SIZE 2048
#define BLOCK_MAX_SIZE 16384

void test_mem_malloc();
void test_free_block();
void test_free_blocks();
void test_new_region();
void test_new_region_in_other_place();