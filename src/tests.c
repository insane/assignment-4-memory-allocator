#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"


static void destroy(void* heap, size_t sz) {
    munmap(heap, size_from_capacity((block_capacity) { .bytes = sz }).bytes); 
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*)(((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void test_mem_malloc(){
    void* heap = heap_init(HEAP_SIZE);
    void* ptr = _malloc(BLOCK_DEF_SIZE);
    if (heap && ptr){
        destroy(heap, HEAP_SIZE);
        fputs("Test 1 passed", stdout);
        return;
    }
    destroy(heap, HEAP_SIZE);
    fputs("Test 1 failed", stderr);
}

void test_free_block() {
    void* heap = heap_init(HEAP_SIZE);
    void* fst = _malloc(BLOCK_DEF_SIZE);

    struct block_header* header = block_get_header(fst);
    _free(fst);
    if (header->is_free) fputs("Test 2 passed", stdout);
    else fputs("Test 2 failed", stderr);

    destroy(heap, HEAP_SIZE);
}

void test_free_blocks() {
    void* heap = heap_init(HEAP_SIZE);
    void* fst = _malloc(BLOCK_DEF_SIZE);
    void* snd = _malloc(BLOCK_DEF_SIZE);

    struct block_header* header1 = block_get_header(fst);
    struct block_header* header2 = block_get_header(snd);
    _free(fst);
    _free(snd);
    if (header1->is_free && header2->is_free) fputs("Test 3 passed", stdout);
    else fputs("Test 3 failed", stderr);

    destroy(heap, HEAP_SIZE);
}

void test_new_region() {
    void* heap = heap_init(HEAP_SIZE);
    void* fst = _malloc(BLOCK_DEF_SIZE);
    void* snd = _malloc(BLOCK_MAX_SIZE);

    if (fst || snd) fputs("Test 4 passed", stdout);
    else  fputs("Test 4 failed", stderr);
    _free(fst);
    _free(snd);        
    destroy(heap, HEAP_SIZE);
}

void test_new_region_in_other_place() {
    void* heap = heap_init(HEAP_SIZE);
    void* fst = _malloc(BLOCK_DEF_SIZE);
    void* snd = _malloc(BLOCK_MAX_SIZE);

    struct block_header* header1 = block_get_header(fst);
    struct block_header* header2 = block_get_header(snd);
    void* result = mmap(header1->next->contents + header1->next->capacity.bytes, HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE , -1, 0);
    if(result != MAP_FAILED || header2 != (void*)(header1->next->contents + header1->next->capacity.bytes)) fputs("Test 5 passed", stdout);
    else  fputs("Test 5 failed", stderr);
    _free(fst);
    _free(snd);
    destroy(heap, HEAP_SIZE);
}